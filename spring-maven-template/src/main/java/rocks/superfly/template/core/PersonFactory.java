package rocks.superfly.template.core;

public class PersonFactory {
	public Person createPerson(int id, String name) {
		System.out.print("Using Factory to create person");
		return new Person(id, name);
	}

}
