package rocks.superfly.template.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("rocks/superfly/template/core/beans/beans.xml");

		Person person = (Person) context.getBean("person");
		person.speak();

		((ClassPathXmlApplicationContext)context).close();

		
	}

}